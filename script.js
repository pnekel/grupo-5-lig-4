//-------------------VARIAVEIS GLOBAIS-------------------
let pedrinha = document.createElement("div"); // cria a pedra
pedrinha.className = "player1"; // Cria a class player e atribui na variavel global

let colunas = document.getElementsByClassName("colunas");

let turn = 1; // armazena de quem é a vez

let button = document.getElementById("btt"); // armazena o botão

let mensagem = document.getElementById("mensagem"); // campo de mensagem
mensagem.innerHTML = "Pac-Man, you start!"; // mensagem inicial

// -------------------1 - CRIANDO O TABULEIRO-------------------
function criarTabuleiro() {

  let table = document.getElementById("table");

  //Loop que cria as colunas
  for (let col = 0; col <= 6; col++) {
    let coluna = document.createElement("div");

    coluna.className = "colunas";
    coluna.id = `column${col}`;

    table.appendChild(coluna);

    //Loop que cria as células
    for (let cel = 1; cel <= 6; cel++) {
      let line = document.getElementById(`column${col}`);
      let celula = document.createElement("div");

      celula.id = `col${col}cel${cel}`;
      celula.className = `col${col} linha${cel}`;
      line.appendChild(celula);
    }
  }
  colunas[0].appendChild(pedrinha); // Append da pedrinha no topo da table
}
criarTabuleiro();

// -------------------2 - DEFINE DE QUAL JOGADOR É A VEZ-------------------
function topoPlayer() {
  //Se o turn for 1, adiciona a classe de player 1
  if (turn === 1) {
    let player = "player1";

    pedrinha.className = player;
    mensagem.innerHTML = "It's Pac-Man's turn!";
    return player;
  }
  //Se o turn for 2, adiciona a classe de player 2
  if (turn === 2) {
    let player = "player2";

    pedrinha.className = player;
    mensagem.innerHTML = "Ghost, it's your turn!";
    return player;
  }
}

// -------------------3 - ALTERA A VEZ PARA O OUTRO PLAYER-------------------
function definindoJogador() {
  // se for a vez do player 1, soma-se 1
  if (turn === 1) {

    let player = "p1";
    turn++;
    return player;
  }
  // se for a vez do player 2, subtrai 1
  if (turn === 2) {

    let player = "p2";
    turn--;
    return player;
  }
}

//-------------------4 - KEYBOARD EVENTS - DEFININDO EVENTOS-------------------
let currentColumn = 0; //Variavel que guarda a coluna atual

document.addEventListener("keydown", function keys(event) {
  let keyName = event.key;
  //Se pressionada seta esquerda, a pedrinha se move diminuindo as colunas
  if (keyName == "ArrowLeft" && currentColumn > 0) {

    currentColumn -= 1;
    colunas[currentColumn].appendChild(pedrinha);
  }
  //Se pressionada seta direita, a pedrinha se move incrementando as colunas
  else if (keyName == "ArrowRight" && currentColumn < 6) {

    currentColumn += 1;
    colunas[currentColumn].appendChild(pedrinha);
    console.log(currentColumn);
  }
  //Se pressionada seta para baixo, ele solta a pedrinha e faz apend na última casa vazia
  else if (keyName == "ArrowDown") {

    let discoNaColuna = document.getElementsByClassName(`col${currentColumn}`);
    console.log(discoNaColuna);

    //Loop que percorre as casas da coluna para pegar a última vazia
    for (let index = 0; index < discoNaColuna.length; index++) {

      if (discoNaColuna[index].className.match("preenchido") === null) {
        discoNaColuna[index].classList.add("preenchido");
        discoNaColuna[index].classList.add(definindoJogador());
        console.log(discoNaColuna[index]);

        topoPlayer();
        diagonalEsquerdaWin();
        diagonalDireitaWin();
        verticalWin();
        condicaoEmpate();
        vitoriaHorizontal();
        return;
      }
      console.log(discoNaColuna[index]);
    }
  }
});

//-------------------5 - Funcionalidade do botão New Game -------------------
button.onclick = function reset() {
  let audio = new Audio('songs/pacman_ringtone.mp3');
  audio.play();
  window.location.reload();
};


// --------------- vertical ------- ------
function verticalWin() {
  let lineClimbing = 0
  let combos = 0
  for (let indice = 0; indice < 7; indice++) {
    let x = document.getElementById(`col${indice}cel${lineClimbing + 1}`).classList[3]
    if (x === 'p1' || x === 'p2') {
      for (let index = 1 + lineClimbing; index < 5 + lineClimbing; index++) {
        let xablau = document.getElementById(`col${indice}cel${index}`).classList[3]
        if (xablau === x) {
          combos++
          if (combos === 4 && xablau === 'p1') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Pac-Man, you won!"; 
          }
          if (combos === 4 && xablau === 'p2') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Ghostie, you won!"; 
          }
        } else {
          combos = 0
          break
        }
      }
    }
    if (indice === 6) {
      lineClimbing++
      indice = -1
      if (lineClimbing === 3) {
        lineClimbing = 0
        break
      }
    }
  }
}

function diagonalEsquerdaWin() {
  let combos = 0
  let lineClimbing = 0
  for (let indice = 0; indice < 4; indice++) {
    let x = document.getElementById(`col${indice}cel${lineClimbing + 1}`).classList[3]
    if (x === 'p1' || x === 'p2') {
      for (let index = 1 + lineClimbing; index < 5 + lineClimbing; index++) {
        let xablau = document.getElementById(`col${indice}cel${index}`).classList[3]
        if (xablau === x) {
          combos++
          indice++
          if (combos === 4 && xablau === 'p1') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Pac-Man, you won!"; 
          }
          if (combos === 4 && xablau === 'p2') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Ghostie, you won!"; 
          }

          
        } else {
          indice -= combos
          combos = 0
          break
        }
      }
      indice -= combos
      combos = 0
    }
    if (indice === 3) {
      lineClimbing++
      indice = -1
      if (lineClimbing === 3) {
        lineClimbing = 0
        break
      }
    }
  }
}

function diagonalDireitaWin() {
  let combos = 0
  let lineClimbing = 0
  for (let indice = 6; indice > 2; indice--) {
    let x = document.getElementById(`col${indice}cel${lineClimbing + 1}`).classList[3]
    if (x === 'p1' || x === 'p2') {
      for (let index = 1 + lineClimbing; index < 5 + lineClimbing; index++) {
        let xablau = document.getElementById(`col${indice}cel${index}`).classList[3]
        if (xablau === x) {
          combos++
          indice--
          if (combos === 4 && xablau === 'p1') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Pac-Man, you won!"; 
          }
          if (combos === 4 && xablau === 'p2') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Ghostie, you won!"; 
          }
        } else {
          indice += combos
          combos = 0
          break
        }
      }
      indice += combos
      combos = 0
    }
    if (indice === 3) {
      lineClimbing++
      indice = 7
      if (lineClimbing === 3) {
        lineClimbing = 0
        break
      }
    }
  }
}

function condicaoEmpate(){
  let totalDivsClassPreenchido = document.getElementsByClassName("preenchido").length;
  //console.log(totalDivsClassPreenchido)

  if(totalDivsClassPreenchido === 42) {

    let msg = document.getElementById("divWin")
            
    msg.innerHTML = "Empate"; 
}
}

function vitoriaHorizontal(){
  let combos = 0
  let lineClimbing = 1
  for (let indice = 0; indice < 4; indice++) {
    let x = document.getElementById(`col${indice}cel${lineClimbing}`).classList[3]
    if (x === 'p1' || x === 'p2') {
      for (let index = indice; index < indice + 4; index++) {
        let xablau = document.getElementById(`col${index}cel${lineClimbing}`).classList[3]
        if (xablau === x) {
          combos++
          if (combos === 4 && xablau === 'p1') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Pac-Man, you won!"; 
          }
          if (combos === 4 && xablau === 'p2') {
            let winner = document.getElementById("divWin")
            
            winner.innerHTML = "Ghostie, you won!"; 
          }
        } else {
          combos = 0
          break
        }
      } 
    }
    if (indice === 3){
      indice = -1
      lineClimbing++
      if (lineClimbing > 6){
        break
      }
    }
  }
}